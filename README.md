# Repositorio-Academico-VisualG
Repositorio Academico dos Algoritmos em Portugol do Software VisualG 

by @paulofreitas.py

Download VisualG
https://sourceforge.net/projects/visualg30/
Sobre o Software
https://visualg3.com.br/




Algoritmo “Ex1"
Faça um algoritmo que receba dois números e exiba o resultado da sua
soma.


// Seção de Declarações

var

         x, y: inteiro

inicio

         // Seção de Comandos

         escreval("Digite o primeiro número: ")

         leia(x)

         escreval("Digite o segundo número: ")

         leia(y)

         escreva("A soma dos números é: ",x+y)

fimalgoritmo

____________________________________________________________________________
Algoritmo “Ex2"
Faça um algoritmo que receba dois números e ao final mostre a soma,
subtração, multiplicação e a divisão dos números lidos.


// Seção de Declarações

var

         x, y: real

inicio

         // Seção de Comandos

         escreva("Digite o primeiro número: ")

         leia(x)

         escreva("Digite o segundo número: ")

         leia(y)

         escreval("A soma é: ",x+y)

         escreval("A subtração é: ",x-y)

         escreval("A multiplicação é: ",x*y)

         escreval("A divisão é: ",x/y)

fimalgoritmo


____________________________________________________________________________
Algoritmo “Ex3"
Escrever um algoritmo que leia o nome de um vendedor, o seu salário
fixo e o total de vendas efetuadas por ele no mês (em dinheiro).
Sabendo que este vendedor ganha 15% de comissão sobre suas vendas
efetuadas, informar o seu nome, o salário fixo e salário no final do mês.


// Seção de Declarações

var

         nome: caractere

         salario: real

         vendas: real

         comissao: real

         salarioFinal: real

inicio

         // Seção de Comandos

         escreval("<><><><><> Sistema de gestão de vendedores <><><><><>")

         escreva(">>> Digite o nome do vendedor: ")

         leia(nome)

         escreva(">>> Digite o salário: ")

         leia(salario)

         escreva(">>> Informe a quantidade de vendas deste no mês: ")

         leia(vendas)

         // Cálculo da comissão e salário final

         comissao <- 0.15 * vendas

         salarioFinal <- salario + comissao

         limpatela

         escreval(">>>>>>>>>> RESUMO <<<<<<<<<<")
         
         escreval("-- Nome: ",nome)

         escreval("-- Salário: ",salario)

         escreval("-- Salário Final (salário + comissão): ",salarioFinal)

         escreval(">>>>>>>>>><><><><><<<<<<<<<<")

fimalgoritmo

____________________________________________________________________________
Algoritmo “Ex4"
Escrever um algoritmo que leia o nome de um aluno e as notas das três
provas que ele obteve no semestre. No final informar o nome do aluno
e a sua média (aritmética).


// Seção de Declarações

var

         aluno: caractere

         notas: vetor[1..3] de real

         x: inteiro

         media: real

inicio

// Seção de Comandos

         escreval("============== Média de alunos ==============")

         escreva("Digite o nome do aluno: ")

         leia(aluno)

         para x de 1 ate 3 faca

                   escreva("Digite a ",x,"º nota ")

                   leia(notas[x])

         fimpara

         media <- (notas[1] + notas[2] + notas[3]) / 3

         limpatela

         escreval("==============", aluno," ==============")

         escreval(">>> Média: ",media)

         escreval("=======================================")

fimalgoritmo

____________________________________________________________________________
Algoritmo “Ex5"
Ler dois valores para as variáveis A e B, e efetuar as trocas dos valores
de forma que a variável A passe a possuir o valor da variável B e a
variável B passe a possuir o valor da variável A. Apresentar os valores
trocados.


// Seção de Declarações

var

         a, b, troca: inteiro



inicio

         // Seção de Comandos

         escreva("Digite o valor(numérico) da variável A: ")

         leia(a)

         escreva("Digite o valor(numérico) da variável B: ")

         leia(b)

         troca <- a

         a <- b

         b <- troca

         limpatela

         escreval("===========================")

         escreval("O novo valor de A é: ",a)

         escreval("O novo valor de B é: ",b)

         escreval("===========================")

fimalgoritmo


____________________________________________________________________________

